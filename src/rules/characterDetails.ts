import { Alignment, IRace } from "../types"

export default class CharacterDetails {
	public static clone(details: CharacterDetails): CharacterDetails {
		return new CharacterDetails(
			details.name,
			details.alignment,
			details.player,
			details.level,
			details.diety,
			details.homeland,
			details.race,
			details.gender,
			details.age,
			details.height,
			details.weight,
			details.hair,
			details.eye,
		)
	}

	public name: string
	public alignment: Alignment
	public player: string
	public level: string[]
	public diety: string
	public homeland: string
	public race: IRace
	public gender: string
	public age: string
	public height: string
	public weight: string
	public hair: string
	public eye: string

	constructor(
		name: string,
		alignment: Alignment,
		player: string,
		level: string[],
		diety: string,
		homeland: string,
		race: IRace,
		gender: string,
		age: string,
		height: string,
		weight: string,
		hair: string,
		eye: string,
	) {
		this.name = name
		this.alignment = alignment
		this.player = player
		this.level = level
		this.diety = diety
		this.homeland = homeland
		this.race = race
		this.gender = gender
		this.age = age
		this.height = height
		this.weight = weight
		this.hair = hair
		this.eye = eye
	}
}

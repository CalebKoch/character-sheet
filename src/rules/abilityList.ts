import { IAbility } from "../types"

const abilityList: IAbility[] = [
	{
		fullName: "Strength",
		id: "Str",
		shortName: "Str",
	},
	{
		fullName: "Constitution",
		id: "Con",
		shortName: "Con",
	},
	{
		fullName: "Dexterity",
		id: "Dex",
		shortName: "Dex",
	},
	{
		fullName: "Wisdom",
		id: "Wis",
		shortName: "Wis",
	},
	{
		fullName: "Inteligence",
		id: "Int",
		shortName: "Int",
	},
	{
		fullName: "Charisma",
		id: "Cha",
		shortName: "Cha",
	},
]

export default abilityList

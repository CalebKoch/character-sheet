import { AbilityScoreIds } from "../types"

export interface ISkillDefinition {
	armorCheckPenalty: boolean,
	keyAbility: AbilityScoreIds,
	name: string,
	trainedOnly: boolean,
}

/***
 * The list of skills. this is not exhaustive at this point.
 */
const skillDefinitionList: ISkillDefinition[] = [
	{
		armorCheckPenalty: true,
		keyAbility: "Dex",
		name: "Acrobatics",
		trainedOnly: false,
	},
	{
		armorCheckPenalty: false,
		keyAbility: "Int",
		name: "Appraise",
		trainedOnly: false,
	},
	{
		armorCheckPenalty: true,
		keyAbility: "Str",
		name: "Climb",
		trainedOnly: false,
	},
	{
		armorCheckPenalty: false,
		keyAbility: "Int",
		name: "Craft",
		// TODO: Need to model the posibility of multiple craft skills
		trainedOnly: false,
	},
	{
		armorCheckPenalty: false,
		keyAbility: "Wis",
		name: "Heal",
		trainedOnly: false,
	},
	{
		armorCheckPenalty: false,
		keyAbility: "Cha",
		name: "Handle Animal",
		trainedOnly: true,
	},
]

export default skillDefinitionList

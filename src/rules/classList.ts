import { IClassDefinition } from "../types"

const classList: IClassDefinition[] = [
	{
		classSkills: [
			"Acrobatics",
			"Climb",
			"Craft",
			"Handle Animal",
			"Intimidate",
			"Knowledge Nature",
			"Ride",
			"Survival",
			"Swim",
		],
		hitDie: 12,
		levelBonus: [{
			baseAttackBonus: 1,
			saves: {
				fort: 2,
				ref: 0,
				will: 0,
			},
			specialEffects: [
				{
					name: "Fast movement",
				},
				{
					name: "Rage",
				},
			],
		}],
		name: "Barbarian",
		skillRanks: 4,
	},
	{
		classSkills: [
			"Appraise",
			"Craft",
			"Diplomacy",
			"Heal",
			"Knowledge Arcana",
			"Knowledge History",
			"Knowledge Nobility",
			"Knowledge Planes",
			"Knowledge Religion",
			"Linguistics",
			"Profession",
			"Sense Motive",
			"Spellcraft",
		],
		hitDie: 8,
		levelBonus: [{
			baseAttackBonus: 0,
			saves: {
				fort: 2,
				ref: 0,
				will: 2,
			},
			specialEffects: [
				{
					name: "Aura",
				},
				{
					name: "Channel Energy (1d6)",
				},
				{
					name: "Domains",
				},
				{
					name: "Orisons",
				},
				{
					name: "Spellcasting",
				},
			],
		}],
		name: "Cleric",
		skillRanks: 2,
	},
]

export default classList

import { IAbility, IClassDefinition, IRaceDefinition } from "../types"
import abilityList from "./abilityList"
import classList from "./classList"
import raceList from "./raceList"

class Rulebook {
	public getAbilityList(): IAbility[] {
		return abilityList
	}

	public getRaces(): IRaceDefinition[] {
		return raceList
	}

	public getClasses(): IClassDefinition[] {
		return classList
	}
}

export default Rulebook

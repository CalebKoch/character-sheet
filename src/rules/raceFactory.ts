import { IOption, IOptionEffects, IRace, IRaceDefinition } from "../types"

export default class RaceFactory {
	public getInstance(definition: IRaceDefinition, selectedOptions: IOption[]) {
		const instance: IRace = {
			abilityScoreAdjustments: [...definition.abilityScoreAdjustments],
			definitionId: definition.id,
			name: definition.name,
			size: definition.size,
		}

		for (const option of selectedOptions) {
			for (const effect of option.effects) {
				this.applyEffect(instance, effect)
			}
		}

		return instance
	}

	public applyEffect(race: IRace, effect: IOptionEffects) {
		// Doing some funky stuff here, so not having it typed is going to be easier, at least for now.
		const untypedRace = (race as any)

		if (effect.action === "add") {
			const property: any = untypedRace[effect.key]
			if (property instanceof Array) {
				untypedRace[effect.key] = [...property, effect.value]
			} else {
				throw new Error(`property ${effect.key} of type ${typeof race} is not an Array. ` +
					`The object is: ${untypedRace[effect.key]}`)
			}
		} else throw new Error(`Action ${effect.action} is not yet supported`)
	}
}

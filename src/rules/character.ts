import { AbilityScores } from "../types"

import CharacterDetails from "./characterDetails"

class Character {
	public static clone(character: Character): Character {
		return new Character(
			character.abilityScores,
			CharacterDetails.clone(character.details),
		)
	}

	public abilityScores: AbilityScores
	public details: CharacterDetails

	constructor(
		baseAbilityScores: AbilityScores,
		details: CharacterDetails,
	) {
		this.abilityScores = baseAbilityScores
		this.details = details
	}
}

export default Character

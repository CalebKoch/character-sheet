import { IAbility, IDecisionDefinition, IRaceDefinition } from "../types"
import abilityList from "./abilityList"

const chooseAbilityScoresOptions: IDecisionDefinition = {
	options: abilityList.map((ability: IAbility) => ({
		displayName: ability.fullName,
		effects: [
			{
				action: "add",
				key: "abilityScoreAdjustments",
				value: {
					ability: ability.id,
					adjustment: 2,
				},
			},
		],
	})),
	question: "Choose an ability score",
}

const raceList: IRaceDefinition[] = [
	{
		abilityScoreAdjustments: [
			{
				ability: "Con",
				adjustment: 2,
			},
			{
				ability: "Wis",
				adjustment: 2,
			},
			{
				ability: "Cha",
				adjustment: -2,
			},
		],
		decisions: [],
		id: "core.race.Dwarves",
		name: "Dwarves",
		size: "Medium",
	},
	{
		abilityScoreAdjustments: [
			{
				ability: "Dex",
				adjustment: 2,
			},
			{
				ability: "Int",
				adjustment: 2,
			},
			{
				ability: "Con",
				adjustment: -2,
			},
		],
		decisions: [],
		id: "core.race.Elf",
		name: "Elf",
		size: "Medium",
	},
	{
		abilityScoreAdjustments: [
			{
				ability: "Con",
				adjustment: 2,
			},
			{
				ability: "Cha",
				adjustment: 2,
			},
			{
				ability: "Str",
				adjustment: -2,
			},
		],
		decisions: [],
		id: "core.race.Gnome",
		name: "Gnome",
		size: "Small",
	},
	{
		abilityScoreAdjustments: [],
		decisions: [chooseAbilityScoresOptions],
		id: "core.race.HalfElf",
		name: "Half-Elf",
		size: "Medium",
	},
	{
		abilityScoreAdjustments: [],
		decisions: [chooseAbilityScoresOptions],
		id: "core.race.HalfOrc",
		name: "Half-Orc",
		size: "Medium",
	},
	{
		abilityScoreAdjustments: [
			{
				ability: "Dex",
				adjustment: 2,
			},
			{
				ability: "Con",
				adjustment: 2,
			},
			{
				ability: "Str",
				adjustment: -2,
			},
		],
		decisions: [],
		id: "core.race.Halfling",
		name: "Halfling",
		size: "Small",
	},
	{
		abilityScoreAdjustments: [],
		decisions: [chooseAbilityScoresOptions],
		id: "core.race.Human",
		name: "Human",
		size: "Medium",
	},
]

export default raceList

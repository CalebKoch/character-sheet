export type AbilityScoreIds = "Str" |
	"Con" |
	"Dex" |
	"Wis" |
	"Int" |
	"Cha"

export type Alignment =
	"LG" | "NG" | "CG" |
	"LN" |  "N" | "CN" |
	"LE" | "NE" | "CE"

export type Size = "Small" | "Medium"

interface IRaceCommon {
	/// The name of the race
	name: string
	size: Size
	abilityScoreAdjustments: {
		adjustment: number,
		ability: AbilityScoreIds,
	}[]

}

export interface IRace extends IRaceCommon {
	definitionId: string
}

export interface IRaceDefinition extends IRaceCommon {
	id: string
	decisions: IDecisionDefinition[]
}

export interface IOption {
	displayName: string
	effects: IOptionEffects[]
}

export interface IOptionEffects {
	// The action that will happen on the key. Currently only add is supported
	action: "add" | "remove"
	// The key that is modified by this effect
	key: string
	// the value that should be added.
	value: any
}

export interface IDecisionDefinition {
	question: string
	options: IOption[]
}

export interface IAbility {
	// the full name of the ability.
	fullName: string
	// the short, three letter abreviation of the ability score.
	shortName: string
	// both shortName and fullName could uniquely identify the ability, but in order to
	// be consistant with the other rules, we will use a similar rule format
	id: AbilityScoreIds
}

export type AbilityScores = {
	[abilityScoreId in AbilityScoreIds]: AbilityScore
}

export class AbilityScore {
	public baseScore: number
	public racialModifier: number
	constructor(baseScore: number, racialModifier: number) {
		this.baseScore = baseScore
		this.racialModifier = racialModifier
	}
	public getScore(): number {
		return this.baseScore + this.racialModifier
	}

	public getModifier(): number {
		return Math.floor((this.getScore() - 10) / 2)
	}
}

export interface ISaves {
	fort: number,
	will: number,
	ref: number,
}

export interface IClassLevelDefinition {
	baseAttackBonus: number,
	saves: ISaves,
	specialEffects: any,
}

export interface IClassDefinition {
	classSkills: string[],
	hitDie: number
	levelBonus: IClassLevelDefinition[],
	name: string,
	skillRanks: number,
}

import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import Rulebook from "./rules/rulebook"

it("renders without crashing", () => {
	const div = document.createElement("div")
	ReactDOM.render(<App rulebook={new Rulebook()} />, div)
	ReactDOM.unmountComponentAtNode(div)
})

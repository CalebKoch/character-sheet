import FormControl from "@material-ui/core/FormControl"
import InputLabel from "@material-ui/core/InputLabel"
import MenuItem from "@material-ui/core/MenuItem"
import Select from "@material-ui/core/Select"
import React, { ChangeEvent } from "react"

interface IGenericSelectProps<TElement> {
	name: string,
	elements: TElement[],
	contentSelector: (item: TElement) => string,
	defaultContent: string,
	selectedElement: TElement,
	onChange: (newItem: TElement) => void,
	id: string,
}

const renderOptions = <TElement extends {}>(props: IGenericSelectProps<TElement>) => {
	const list: (TElement | null)[] = [null, ...props.elements]

	return list.map((value: TElement | null, index) => {
		if (index === 0) {
			return <MenuItem value="-1" key="-1">
				<em>
					{props.defaultContent || "Make a selection"}
				</em>
			</MenuItem>
		} else {
			const element: TElement = value as TElement
			return (
				// index includes the null value we inserted at the beginning.
				// Subtract one, so value is the original index.
				<MenuItem value={index - 1} key={index - 1}>
					{props.contentSelector(element)}
				</MenuItem>
			)
		}
	})
}

const handleChange = <TElement extends{}>(props: IGenericSelectProps<TElement>) =>
	(event: ChangeEvent<HTMLSelectElement>) => {
		const index = Number(event.target.value)
		props.onChange(props.elements[index])
	}

const GenericSelect = <TElement extends {}>(props: IGenericSelectProps<TElement>) => {
	return (
		<FormControl id={props.id}>
			<InputLabel htmlFor={props.id + "-select"}>{props.name}</InputLabel>
			<Select
				inputProps={{id: props.id + "-select"}}
				onChange={handleChange(props)}
				value={props.elements.indexOf(props.selectedElement)}
			>
				{renderOptions<TElement>(props)}
			</Select>
		</FormControl>
	)
}

export default GenericSelect

import Card from "@material-ui/core/Card"
import Checkbox from "@material-ui/core/Checkbox"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Typography from "@material-ui/core/Typography"
import React, { ChangeEvent } from "react"

interface ICheckboxes {
	[key: string]: boolean,
}

interface IDebugCardProps {
	checkboxes: ICheckboxes,
	onCheckboxChange: ChangeHandler,
}

type ChangeHandler = (key: string) => (event: ChangeEvent<HTMLInputElement>) => void

const renderSingleCheckbox = (
	key: string,
	value: boolean,
	onChange: ChangeHandler) => (

		<FormControlLabel
			key={key}
			control = {
				<Checkbox
					checked={value}
					onChange={onChange(key)}
				/>
			}
		label={key}
	/>
)

const renderCheckboxes = (checkboxes: ICheckboxes, onChange: ChangeHandler) => (
	Object.keys(checkboxes).map((key: string) => renderSingleCheckbox(key, checkboxes[key], onChange))
)

const DebugCard = (props: IDebugCardProps) => {
	return (
		<Card >
			<Typography>Eventually this will contain the logo. For now, it is just a spot for some settings.</Typography>
			{renderCheckboxes(props.checkboxes, props.onCheckboxChange)}
		</Card>
	)
}

export default DebugCard

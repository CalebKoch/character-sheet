import Card from "@material-ui/core/Card"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Tooltip from "@material-ui/core/Tooltip"
import React, { ChangeEvent, Component } from "react"

import CharacterDetails from "../rules/characterDetails"
import RaceFactory from "../rules/raceFactory"
import Rulebook from "../rules/rulebook"
import { IClassDefinition, IOption, IRaceDefinition } from "../types"
import DecisionDialog from "./DecisonDialog"
import GenericSelect from "./GenericSelect"

export interface IDescriptionCardProps {
	details: CharacterDetails,
	rulebook: Rulebook,
	onChange: (details: CharacterDetails) => void,
	edit: boolean,
}

interface IDescriptionCardState {
	decisionDialogOpen: boolean,
	selectedRace?: IRaceDefinition
}

class DescriptionCard extends Component<IDescriptionCardProps, IDescriptionCardState> {
	private factory: RaceFactory
	constructor(props: IDescriptionCardProps) {
		super(props)
		this.state = {
			decisionDialogOpen: false,
		}
		this.factory = new RaceFactory()
	}

	public render() {
		return (
			<Card>
				<Grid
					container
					spacing={8}
					alignItems={"flex-start"}
					justify={"flex-start"}
				>
					<Grid item xl={6}>
						<TextField
							label="Character Name"
							value={this.props.details.name}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({name: event.target.value})}
						/>
					</Grid>
					<Grid item xl={2}>
						<TextField
							label="Alignment"
							value={this.props.details.alignment}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
						/>
					</Grid>
					<Grid item xl={4}>
						<TextField
							label="Player"
							value={this.props.details.player}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({player: event.target.value})}
						/>
					</Grid>
					<Grid item xl={8}>
						{this.renderClassSelect()}
					</Grid>
					<Grid item xl={2}>
						<TextField
							label="Diety"
							value={this.props.details.diety}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({diety: event.target.value})}
						/>
					</Grid>
					<Grid item xl={2}>
						<TextField
							label="Homeland"
							value={this.props.details.homeland}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({homeland: event.target.value})}
						/>
					</Grid>
					<Grid item xl={5}>
						{this.renderRaceSelect()}
					</Grid>
					<Grid item xl={1}>
						<Tooltip title="Size is determined by your race">
							<TextField
								label="Size"
								value={this.props.details.race.size}
								InputProps={{disableUnderline: !this.props.edit, disabled: true}}
							/>

						</Tooltip>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Gender"
							value={this.props.details.gender}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({gender: event.target.value})}
						/>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Age"
							value={this.props.details.age}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({age: event.target.value})}
						/>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Height"
							value={this.props.details.height}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({height: event.target.value})}
						/>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Weight"
							value={this.props.details.weight}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({weight: event.target.value})}
						/>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Hair"
							value={this.props.details.hair}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({hair: event.target.value})}
						/>
					</Grid>
					<Grid item xl={1}>
						<TextField
							label="Eye"
							value={this.props.details.eye}
							InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
							onChange={(event: ChangeEvent<HTMLInputElement>) => this.handleChange({eye: event.target.value})}
						/>
					</Grid>
				</Grid>
				<DecisionDialog
					definition={this.state.selectedRace ? this.state.selectedRace.decisions : []}
					open={!!this.state.selectedRace}
					onClose={this.handleRaceDialogClose}
					title={`Make your decisions for the race ${this.state.selectedRace ? this.state.selectedRace.name : ""}`}
				/>
			</Card>
		)
	}

	private levelsArrayToDictionary(levelsArray: string[]): { [classId: string]: number } {
		const dict: { [classId: string]: number } = {}
		for (const classId of levelsArray) {
			if (dict[classId]) {
				dict[classId]++
			} else {
				dict[classId] = 1
			}
		}
		return dict

	}

	private renderClassSelect = () => {
		const definition = this.props.rulebook.getClasses()
			.find(value => value.name === this.props.details.level[0])

		if (!definition) throw Error(`No class with name ${this.props.details.level[0]} found`)

		const levelDictionary = this.levelsArrayToDictionary(this.props.details.level)
		let levelString: string | null = null
		for (const classId in levelDictionary) {
			if (levelDictionary.hasOwnProperty(classId)) {
				if (!levelString) {
					levelString = ""
				} else {
					levelString += ","
				}
				levelString = `${classId} (${levelDictionary[classId]})`
			}
		}
		if (this.props.edit) {
			return (
				<GenericSelect
					name="Character Level"
					elements={this.props.rulebook.getClasses()}
					contentSelector={(classDefinition: IClassDefinition) => classDefinition.name }
					defaultContent="Choose a Class"
					selectedElement={definition}
					onChange={(newItem: IClassDefinition) => this.handleChange({level: [newItem.name]})}
					id="class"
				/>
			)
		} else {
			return (
				<TextField
					label="Character Level"
					value={levelString || ""}
					InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
				/>
			)
		}
	}

	private renderRaceSelect = () => {
		const definition = this.props.rulebook.getRaces()
			.find(value => value.id === this.props.details.race.definitionId)

		if (!definition) throw Error(`No race with id ${this.props.details.race.definitionId} found`)

		if (this.props.edit) {
			return <GenericSelect
				name="Race"
				elements={this.props.rulebook.getRaces()}
				contentSelector={(race: IRaceDefinition) => race.name }
				defaultContent="Choose a race"
				selectedElement={definition}
				onChange={(newItem: IRaceDefinition) => this.handleRaceSelectChange(newItem)}
				id="race"
			/>
		} else {
			return <TextField
						label="Race"
						value={this.props.details.race.name}
						InputProps={{disableUnderline: !this.props.edit, disabled: !this.props.edit}}
					/>
		}
	}

	// Once the user selects a race, we may need more input before we can set their new race.
	// Don't use the regular handleChange just yet.
	private handleRaceSelectChange = (race: IRaceDefinition) => {
		if (race.decisions.length > 0) {
			this.setState({
				decisionDialogOpen: true,
				selectedRace: race,
			})
		} else {
			this.handleChange({race: this.factory.getInstance(race, [])})
		}
	}

	private handleRaceDialogClose = (selectedValues: IOption[]) => {
		const raceDefinition = this.state.selectedRace

		if (!raceDefinition) {
			throw Error("handeling dialog close, while race is undefined")
		}

		const raceFactory = new RaceFactory()
		const race = raceFactory.getInstance(raceDefinition, selectedValues)
		this.handleChange({
			race,
		})
		this.setState({selectedRace: undefined})
	}

	private handleChange = (newDetails: Partial<CharacterDetails>) => {
		if (this.props.edit) {
			const clonedDetails = CharacterDetails.clone(this.props.details)
			this.props.onChange({...clonedDetails, ...newDetails})
		}
	}
}

export default DescriptionCard

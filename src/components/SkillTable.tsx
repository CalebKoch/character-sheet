import Card from "@material-ui/core/Card"
import Checkbox from "@material-ui/core/Checkbox"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import React, { Component } from "react"

import Character from "../rules/character"
import Rulebook from "../rules/rulebook"
import skills, { ISkillDefinition } from "../rules/skillList"
import { AbilityScores } from "../types"

export interface ISkillTableProps {
	character: Character
	rulebook: Rulebook
}

class SkillTable extends Component<ISkillTableProps, {}> {
	public render() {
		const classSkills = this.getClassSkills()

		return (
			<Card>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell>Class Skill</TableCell>
							<TableCell>Skill Name</TableCell>
							<TableCell>Total Bonus</TableCell>
							<TableCell></TableCell>
							<TableCell>Ability Mod.</TableCell>
							<TableCell>Ranks</TableCell>
							<TableCell>Misc. Mod.</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{skills.map(skill => this.renderSkillRow(skill, classSkills))}
					</TableBody>
				</Table>
			</Card>
		)
	}

	private renderSkillRow = (skill: ISkillDefinition, classSkills: {[skill: string]: true}) => {
		let nameDisplay = skill.name
		if (skill.trainedOnly) {
			nameDisplay += "*"
		}

		const abilityModifier = this.props.character.abilityScores[skill.keyAbility].getModifier()
		const total = abilityModifier
		const isClassSkill = classSkills[skill.name]

		return (
			<TableRow key={skill.name} >
				<TableCell><Checkbox
					checked={!!isClassSkill}
				/></TableCell>
				<TableCell>{nameDisplay}</TableCell>
				<TableCell>{total}</TableCell>
				<TableCell>={skill.keyAbility}</TableCell>
				<TableCell>{abilityModifier}</TableCell>
				<TableCell>{0}</TableCell>
				<TableCell>{0}</TableCell>
			</TableRow>
		)
	}

	private getClassSkills = (): {[skill: string]: true} => {
		const classes: {[className: string]: true} = {}
		// If we see this class again, we already know the class skills.
		const classSkills: {[skill: string]: true} = {}

		for (const className of this.props.character.details.level) {
			if (classes[className]) break

			classes[className] = true
			const classDefinition = this.props.rulebook.getClasses().find(value => value.name === className)

			if (!classDefinition) break

			for (const skillName of classDefinition.classSkills) {
				classSkills[skillName] = true
			}
		}
		return classSkills
	}
}

export default SkillTable

import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import DialogTitle from "@material-ui/core/DialogTitle"
import FormControl from "@material-ui/core/FormControl"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import List from "@material-ui/core/List"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import Typography from "@material-ui/core/Typography"

import React from "react"
import { IDecisionDefinition, IOption } from "../types"

export interface IProps {
	definition: IDecisionDefinition[]
	open: boolean
	onClose: (selectedValues: IOption[]) => void
	title: string
}

export interface IState {
	selectedOptions: IOption[]
	currentIndex: number
}

export default class DecisionDialog extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props)
		this.state = {
			currentIndex: -1,
			selectedOptions: [],
		}
	}

	public render() {
		if (this.props.definition.length === 0) {

			return null
		}
		const { onClose, definition, ...other } = this.props
		const question = definition[0].question

		// TODO: Eventually, we will need to put a loop around some of this to handle multiple decisions.
		// Probably want to use a Stepper at that point as well.
		return (
			<Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
				<DialogTitle id="simple-dialog-title">{this.props.title}</DialogTitle>
				<div>
					<Typography>{question}</Typography>
					<List>
						<FormControl>
							<RadioGroup
								aria-label="Gender"
								name={question}
								value={this.state.currentIndex + ""}
								onChange={this.handleChange}
							>
								{definition[0].options.map((option, index) => (
									<FormControlLabel key={option.displayName} value={index + ""} control={<Radio />} label={option.displayName} />
								))}
							</RadioGroup>
						</FormControl>
					</List>
					<Button onClick={this.handleOk} >
						OK
					</Button>
				</div>
			</Dialog>
		)
	}

	private handleOk = () => {
		const definition = this.props.definition[0]
		const selectedOption = definition.options[this.state.currentIndex]
		this.handleClose(undefined, selectedOption)
	}

	private handleClose = (event?: React.SyntheticEvent<{}>, lastOption?: IOption) => {
		// Since setting state is async, we can't add the last option to state, and then call onClose.
		// So just add it to a new list here.
		let allOptions: IOption[]
		if (lastOption) {
			allOptions = [...this.state.selectedOptions, lastOption]
		} else {
			allOptions = [...this.state.selectedOptions]
		}
		this.props.onClose(allOptions)

		// reset the state.
		this.setState({
			currentIndex: -1,
			selectedOptions: [],
		})
	}

	private handleChange = (event: React.ChangeEvent<{}>, value: string) => {
		this.setState({
			currentIndex: Number(value),
		})
	}

}

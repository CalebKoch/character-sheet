import Card from "@material-ui/core/Card"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import React, { ChangeEvent, Component } from "react"

import Character from "../rules/character"
import RuleBook from "../rules/rulebook"
import { AbilityScoreIds, IAbility } from "../types"

export interface IAbilityTableProps {
	rulebook: RuleBook
	edit: boolean
	character: Character
	changeScore: (abilityId: AbilityScoreIds, score: number) => void
}

interface IAbilityTableState {
	// In the future, this will include the current values, so if an edit is
	// canceled, we can revert to the state in the props.
}

const lookupPointBuy = (score: number) => {
	switch (score) {
		case 7: return -4
		case 8: return -2
		case 9: return -1
		case 10: return 0
		case 11: return 1
		case 12: return 2
		case 13: return 3
		case 14: return 5
		case 15: return 7
		case 16: return 10
		case 17: return 13
		case 18: return 17
		default:
			// You shouldn't be able to go below 7, or above 18 in this.
			// For now, we just set the point buy values, to not give a benifit if lower than 7,
			// or to something that is going to be unobtainable if higher than 18.
			return score < 10 ? -4 : 100
	}
}

class AbilityTable extends Component<IAbilityTableProps, IAbilityTableState> {
	constructor(props: IAbilityTableProps) {
		super(props)
		this.state = {}
	}

	public render() {
		let pointBuyTotal: number = 0

		for (const ability of this.props.rulebook.getAbilityList()) {
			const abilityScore = this.props.character.abilityScores[ability.id]
			pointBuyTotal += lookupPointBuy(abilityScore.baseScore)
		}
		return (
			<Card>
				<Table>
					<TableHead>
						{
							this.props.edit ?
								this.renderEditHeaderRow() :
								this.renderNoEditHeaderRow()
						}
					</TableHead>
					<TableBody>
						{
							this.props.rulebook.getAbilityList().map(
								this.props.edit ?
									this.renderEditAbilityRow :
									this.renderNoEditAbilityRow,
							)
						}
					</TableBody>
				</Table>
				{(() => {
					if (this.props.edit) {
						return <Typography>Total PointBuy: {pointBuyTotal}</Typography>
					}
				})()}
			</Card>
		)
	}

	private renderNoEditHeaderRow = () => (
		<TableRow>
			<TableCell>Ability</TableCell>
			<TableCell>Score</TableCell>
			<TableCell>Modifier</TableCell>
			<TableCell>Temp. Adj.</TableCell>
			<TableCell>Temp. Mod.</TableCell>
		</TableRow>
	)

	private renderNoEditAbilityRow = (ability: IAbility ) => {
		const { id } = ability
		const abilityScore = this.props.character.abilityScores[id]
		const baseScore: number = abilityScore.baseScore
		const racialAdjustment: number = abilityScore.racialModifier
		const modifier: number = abilityScore.getModifier()
		const score: number = abilityScore.getScore()

		return (
			<TableRow key={id} >
				<TableCell>{ability.shortName}</TableCell>
				<TableCell>{score}</TableCell>
				<TableCell>{modifier}</TableCell>
				<TableCell>0</TableCell>
				<TableCell>{modifier}</TableCell>
			</TableRow>
		)
	}

	private renderEditHeaderRow = () => (
		<TableRow>
			<TableCell>Ability</TableCell>
			<TableCell>Base Score</TableCell>
			<TableCell>Point Buy</TableCell>
			<TableCell>Racial Adj.</TableCell>
			<TableCell>Modifier</TableCell>
		</TableRow>
	)

	private renderEditAbilityRow = (ability: IAbility ) => {
		const { id } = ability

		const abilityScore = this.props.character.abilityScores[id]
		const baseScore: number = abilityScore.baseScore
		const racialAdjustment: number = abilityScore.racialModifier
		const modifier: number = abilityScore.getModifier()

		return (
			<TableRow key={id}>
				<TableCell>{ability.shortName}</TableCell>
				<TableCell>
				<TextField
					id="standard-number"
					value={baseScore}
					onChange={(event: ChangeEvent<HTMLInputElement>) => this.props.changeScore(id, Number(event.target.value))}
					type="number"
					InputLabelProps={{
						shrink: true,
					}}
				/>
				</TableCell>
				<TableCell>{lookupPointBuy(baseScore)}</TableCell>
				<TableCell>{racialAdjustment}</TableCell>
				<TableCell>{modifier}</TableCell>
			</TableRow>
		)
	}

	private getRacialAdjustment = (ability: IAbility): number => {
		const racialAdjustments = this.props.character.details.race.abilityScoreAdjustments
		for (const adjustment of racialAdjustments) {
			if (adjustment.ability === ability.id) {
				return adjustment.adjustment
			}
		}
		return 0
	}
}

export default AbilityTable

import Card from "@material-ui/core/Card"
import Grid from "@material-ui/core/Grid"
import React, { ChangeEvent, Component } from "react"

import "./App.css"
import AbilityTable from "./components/AbilityTable"
import DebugCard from "./components/DebugCard"
import DescriptionCard from "./components/DescriptionCard"
import NavBar from "./components/NavBar"
import SkillTable from "./components/SkillTable"
import Character from "./rules/character"
import CharacterDetails from "./rules/characterDetails"
import RaceFactory from "./rules/raceFactory"
import Rulebook from "./rules/rulebook"
import { AbilityScore, AbilityScoreIds, AbilityScores} from "./types"

interface IAppState {
	character: Character
	edit: boolean
}

interface IAppProps {
	rulebook: Rulebook
}

class App extends Component<IAppProps, IAppState> {
	constructor(props: IAppProps) {
		super(props)

		const factory = new RaceFactory()
		const race = factory.getInstance(this.props.rulebook.getRaces()[1], [])

		const scores: {[ability: string]: AbilityScore} = {}
		let counter = 7
		for (const ability of props.rulebook.getAbilityList()) {
			const racialAdjustment = race.abilityScoreAdjustments.find(value => value.ability === ability.id)
			const adjustmentValue = racialAdjustment ? racialAdjustment.adjustment : 0
			scores[ability.id] = new AbilityScore(counter, adjustmentValue)
			counter++
		}

		const details: CharacterDetails = new CharacterDetails(
			"Filler Character",
			"LG",
			"Filler Player",
			["Barbarian", "Fake Class"],
			"",
			"",
			factory.getInstance(this.props.rulebook.getRaces()[1], []),
			"Male",
			"",
			"",
			"",
			"",
			"",
		)

		this.state = {
			character: new Character(
				scores as AbilityScores,
				details,
			),
			edit: true,
		}
	}

	public render() {
		return (
			<div>
				<NavBar />
				<Grid
					container
					spacing={16}
					alignItems={"flex-start"}
					justify={"flex-start"}
				>
					<Grid item xl={4}>
						<DebugCard checkboxes={{edit: this.state.edit}} onCheckboxChange={this.onCheckboxChange}/>
					</Grid>
					<Grid item xl={8}>
						<DescriptionCard
							details={this.state.character.details}
							rulebook={this.props.rulebook}
							onChange={(details: CharacterDetails) => this.onChange({details})}
							edit={this.state.edit}
						/>
					</Grid>
					<Grid item xl={12} >
						<Grid container
							spacing={16}
							alignItems={"flex-start"}
							justify={"flex-start"}
						>
							<Grid item xl={7} >
								<Grid container
									spacing={16}
									alignItems={"flex-start"}
									justify={"flex-start"}
								>
									<Grid item xl={6}>
										<AbilityTable
											rulebook={this.props.rulebook}
											edit={this.state.edit}
											character={this.state.character}
											changeScore={this.onAbilityChange}
										/>
									</Grid>
									<Grid item xl={6}>
										<Grid
											container
											spacing={16}
											alignItems={"flex-start"}
											justify={"flex-start"}
										>
											<Grid item xl={12} >
												<Card>This is where HP Goes</Card>
											</Grid>
											<Grid item xl={12} >
												<Card>followed by Wounds</Card>
											</Grid>
											<Grid item xl={12} >
												<Card>And then Nonlethal Damage</Card>
											</Grid>
											<Grid item xl={12} >
												<Card>this is initiative</Card>
											</Grid>
										</Grid>
									</Grid>
									<Grid item xl={12} >
										<Card>this is Ac</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>Flatfooted and touch</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>Saving Throws</Card>
									</Grid>
									<Grid item xl={8} >
										<Card>Base Atack Bonus</Card>
									</Grid>
									<Grid item xl={4} >
										<Card>Spell Resistance</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>CMB</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>CMD</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>Weapon</Card>
									</Grid>
									<Grid item xl={12} >
										<Card>Weapon</Card>
									</Grid>
								</Grid>
							</Grid>
							<Grid item xl={5}>
								<Grid
									container
									spacing={16}
									alignItems={"flex-start"}
									justify={"flex-start"}
								>
									<Grid item xl={12}>
										<Card>This will be speed</Card>
									</Grid>
									<Grid item xl={12}>
										<SkillTable
											character={this.state.character}
											rulebook={this.props.rulebook}/>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</div>
		)
	}

	private onAbilityChange = (abilityId: AbilityScoreIds, score: number) => {
		const clonedCharacter = Character.clone(this.state.character)
		clonedCharacter.abilityScores[abilityId].baseScore = score
		this.setState({character: clonedCharacter})
	}

	private onChange = (newDetails: Partial<Character>) => {
		const clonedCharacter = Character.clone(this.state.character)
		const currentRace = clonedCharacter.details.race
		const newRace = newDetails.details && newDetails.details.race

		if (newRace && currentRace !== newRace) {
			// reset the old adjustments
			for (const { ability } of currentRace.abilityScoreAdjustments) {
				clonedCharacter.abilityScores[ability].racialModifier = 0
			}
			for (const { ability, adjustment } of newRace.abilityScoreAdjustments) {
				clonedCharacter.abilityScores[ability].racialModifier = adjustment
			}
		}

		this.setState({character: {...clonedCharacter, ...newDetails}})
	}

	private onCheckboxChange = (key: string) => (event: ChangeEvent<HTMLInputElement>): void => {
		if (key === "edit") {
			this.setState({edit: ! this.state.edit})
		}
	}
}

export default App
